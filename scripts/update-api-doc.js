const yaml = require('js-yaml');
const fs = require('fs');

const yamlInput = 'openapi.yaml';
const output = './api-doc/load-swagger-config.js';

const jsonConfig = yaml.load(fs.readFileSync(yamlInput, 'utf-8'));
fs.writeFileSync(output, 'let swaggerConfig = ' + JSON.stringify(jsonConfig));
