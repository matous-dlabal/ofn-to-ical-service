module.exports = function(grunt) {
  grunt.initConfig({
    eslint: {
      options: {
        fix: grunt.option('fix'), // this will get params from the flags
        ignorePattern: 'src/lib/*',
      },
      target: ['src/**/*.js', 'Gruntfile.js'],
    },
  });

  grunt.loadNpmTasks('grunt-eslint');

  grunt.registerTask('default', ['lint']);
  grunt.registerTask('lint', ['eslint']);
};
