const express = require('express');
const cors = require('cors');
const app = express();
const OFNCAL = require('./lib/ofn-to-ical-lib.min.js');

app.set('PORT', process.env.PORT || 8080);

app.use(cors());
app.use(express.text({type: 'application/ld+json'}));

function sendError(res, msg, code = 400) {
  if (msg === undefined) {
    return res.sendStatus(code);
  }
  return res.status(code).send({error: msg});
}

function checkRequest(req, res, next) {
  // accept only application/ld+json
  if (req.headers['content-type'] !== 'application/ld+json') {
    return sendError(res, undefined, 415);
  }

  // body is required
  // with the correct content type non empty body is of type string
  if (typeof req.body === 'object') {
    return sendError(res, 'Body required');
  }

  // accept-language is required
  if (!req.headers['accept-language']) {
    return sendError(res, 'Accept-Language required');
  }

  next();
}

app.post('/api/kalend%C3%A1%C5%99/v%C3%ADce', checkRequest);
app.post('/api/kalend%C3%A1%C5%99/jeden', checkRequest);

app.post('/api/kalend%C3%A1%C5%99/v%C3%ADce', (req, res) => {
  const langCode = req.headers['accept-language'];
  try {
    const icals = OFNCAL.generate(req.body, langCode, false);
    res.send(icals);
  } catch (e) {
    sendError(res, e.message);
  }
});

app.post('/api/kalend%C3%A1%C5%99/jeden', (req, res) => {
  const langCode = req.headers['accept-language'];
  try {
    const icals = OFNCAL.generate(req.body, langCode, true);
    res.setHeader('Content-Type', 'text/calendar');
    res.send(icals[0]);
  } catch (e) {
    sendError(res, e.message);
  }
});


// Start the server
app.listen(app.get('PORT'), () => {
  console.log(`Server listening on port ${app.get('PORT')}`);
});
