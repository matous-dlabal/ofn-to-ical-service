# Generování událostí iCal z dat dle OFN - HTTP služba

HTTP služba pro generování událostí ve formátu iCalendar z dat dle OFN.
Součást bakalářské práce
*Generování kalendářových událostí z dat dle Otevřených formálních norem*
vytvořené v rámci studia
na [Fakultě Informačních Technologií ČVUT](https://fit.cvut.cz).

## Vyzkoušení služby

Služba je dostupná na adrese
`https://ofn-to-ical-service.herokuapp.com/`.

V rámci práce byla vytvořena
i [webová stránka](https://matous-dlabal.gitlab.io/ofn-to-ical-web/),
která nabízí GUI pro praktické použití služby.

## Dokumentace

Programátorská dokumentace se nachází v textu práce v kapitole
[7.1.2](https://matous-dlabal.gitlab.io/ofn-to-ical-web/bakalarska-prace-matous-dlabal.pdf#56).

[Interaktivní dokumentace API](https://matous-dlabal.gitlab.io/ofn-to-ical-service/api-doc/)
zobrazuje dokumentaci ze souboru `openapi.yaml` pomocí nástroje Swagger UI.

